package entity;

import java.sql.Date;
import java.time.LocalDate;

public class JobOrder {
    private int joborder_id;
    private int client_id;
    private int approved_by;
    private Date request_date;
    private Date approved_date;
    private double est_cost;

    public JobOrder() {

    }

    public JobOrder(int joborder_id, int client_id, int approved_by,
                    Date request_date, Date approved_date,
                    double est_cost) {
        this.joborder_id = joborder_id;
        this.client_id = client_id;
        this.approved_by = approved_by;
        this.request_date = request_date;
        this.approved_date = approved_date;
        this.est_cost = est_cost;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public void setApproved_by(int approved_by) {
        this.approved_by = approved_by;
    }

    public void setApproved_date(Date approved_date) {
        this.approved_date = approved_date;
    }

    public void setEst_cost(double est_cost) {
        this.est_cost = est_cost;
    }

    public void setJoborder_id(int joborder_id) {
        this.joborder_id = joborder_id;
    }

    public void setRequest_date(Date request_date) {
        this.request_date = request_date;
    }

    public int getClient_id() {
        return client_id;
    }

    public double getEst_cost() {
        return est_cost;
    }

    public int getApproved_by() {
        return approved_by;
    }

    public int getJoborder_id() {
        return joborder_id;
    }

    public Date getApproved_date() {
        return approved_date;
    }

    public Date getRequest_date() {
        return request_date;
    }

}