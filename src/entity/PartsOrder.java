package entity;

import java.sql.Date;
import java.time.LocalDate;

public class PartsOrder {
    private int partsorder_id;
    private int joborder_id;
    private Date order_date;
    private Date arrive_date;

    public PartsOrder() {

    }

    public PartsOrder(int partsorder_id, int joborder_id,
                      Date order_date, Date arrive_date) {
        this.partsorder_id = partsorder_id;
        this.joborder_id = joborder_id;
        if(order_date == null && arrive_date == null){
            this.order_date = null;
            this.arrive_date = null;
        }else if(order_date != null && arrive_date == null){
            this.order_date = order_date;
            this.arrive_date = null;
        }else if(order_date == null && arrive_date != null){
            this.order_date = null;
            this.arrive_date = arrive_date;
        }else if(arrive_date != null && order_date != null){
            this.order_date = order_date;
            this.arrive_date = arrive_date;
        }

    }
    public int getPartsorder_id(){return partsorder_id;}
    public void setPartsorder_id(int partsorder_id){this.partsorder_id=partsorder_id;}
    public int getJoborder_id(){return joborder_id;}
    public void setJoborder_id(int joborder_id){this.joborder_id = joborder_id;}
    public Date getOrder_date(){return order_date;}
    public void setOrder_date(Date order_date){this.order_date = order_date;}
    public Date getArrive_date(){return arrive_date;}
    public void setArrive_date(Date arrive_date){this.arrive_date = arrive_date;}

    public String toString(){
        return String.format("%-20s%-20s%-20s%-20s%n", partsorder_id, joborder_id, order_date, arrive_date);
    }
}
