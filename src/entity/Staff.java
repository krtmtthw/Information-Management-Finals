package entity;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Staff {
    private List<Staff> staffList;
    private int staff_id;
    private String name;
    private int role;
    private LocalDate hire_date;
    private LocalDate termination_date;

    public Staff( int staff_id, String name, int role, String hire_date, String termination_date ) {
        this.staff_id = staff_id;
        this.name = name;
        this.role = role;
        if(hire_date == null && termination_date == null){
            this.hire_date = null;
            this.termination_date = null;
        }else if(hire_date != null && termination_date == null){
            this.hire_date = LocalDate.parse(hire_date);
            this.termination_date = null;
        }else if(hire_date == null && termination_date != null){
            this.hire_date = null;
            this.termination_date = LocalDate.parse(termination_date);
        }else if(hire_date != null && termination_date != null){
            this.hire_date = LocalDate.parse(hire_date);
            this.termination_date = LocalDate.parse(termination_date);
        }

    }

    public Staff(int staff_id, String name, int role) {
        this(staff_id, name, role, null, null);
    }

    // constructor for newly hired staff
    // without termination_date
    // use -1 for role if no role yet
    public Staff(int staff_id, String name, int role, LocalDate hire_date) {
        this.staff_id = staff_id;
        this.name = name;
        this.role = role;
        this.hire_date = hire_date;
    }

    public Staff( List<Staff> staffList){
        this.staffList = staffList;
    }
    public int getStaff_id() {return staff_id;}
    public String getName() {
        return name;
    }
    public int getRole() {
        return role;
    }
    public void setStaffList(List<Staff> staffList){this.staffList = staffList;}
    public void setRole(int role){this.role = role;}
    public void setHire_Date(LocalDate hire_date){this.hire_date = hire_date;}
    public LocalDate getHire_date(){return hire_date;}
    public void setTermination_date(LocalDate termination_date){this.termination_date = termination_date;}
    public LocalDate getTermination_date(){return termination_date;}
    public String toString(){
        String format = "";
        String terminationString;
        if (termination_date == null) terminationString = "N/A";
        else terminationString = termination_date.toString();
        format = String.format("%-13s%-13s%-20s%-15s%-15s%n", staff_id, role, name, hire_date, terminationString);
        return format;
    }



}
