package entity;

public class StocksToInvItem {
    private int partsorder_id;
    private int part_id;
    private int quantity_arrived;

    public StocksToInvItem() {

    }

    public StocksToInvItem(int partsorder_id, int part_id,
                           int quantity_arrived) {
        this.partsorder_id = partsorder_id;
        this.part_id = part_id;
        this.quantity_arrived = quantity_arrived;
    }

    public int getPart_id() {
        return part_id;
    }

    public int getPartsorder_id() {
        return partsorder_id;
    }

    public int getQuantity_arrived() {
        return quantity_arrived;
    }

    public void setPart_id(int part_id) {
        this.part_id = part_id;
    }

    public void setPartsorder_id(int partsorder_id) {
        this.partsorder_id = partsorder_id;
    }

    public void setQuantity_arrived(int quantity_arrived) {
        this.quantity_arrived = quantity_arrived;
    }
}
