package entity;

import java.time.LocalDate;

public class Receipt {
    private int joborder_id;
    private int payment_processor;
    private LocalDate pay_date;
    private double fin_cost;

    public Receipt() {

    }

    public Receipt(int joborder_id, int payment_processor,
                   LocalDate pay_date, double fin_cost) {
        this.joborder_id = joborder_id;
        this.payment_processor = payment_processor;
        this.pay_date = pay_date;
        this.fin_cost = fin_cost;
    }

    public void setJoborder_id(int joborder_id) {
        this.joborder_id = joborder_id;
    }

    public void setFin_cost(double fin_cost) {
        this.fin_cost = fin_cost;
    }

    public void setPay_date(LocalDate pay_date) {
        this.pay_date = pay_date;
    }

    public void setPayment_processor(int payment_processor) {
        this.payment_processor = payment_processor;
    }

    public int getJoborder_id() {
        return joborder_id;
    }

    public double getFin_cost() {
        return fin_cost;
    }

    public int getPayment_processor() {
        return payment_processor;
    }

    public LocalDate getPay_date() {
        return pay_date;
    }
}
