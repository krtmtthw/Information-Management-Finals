package entity;

import java.sql.Date;
import java.util.List;

public class Client {
    private int client_id;
    private String name;
    private String contact_number;
    private List<Client> clientList;

    public Client(int client_id, String name, String contact_number) {
        this.client_id = client_id;
        this.name = name;
        this.contact_number = contact_number;
    }
    public Client(List<Client> clientList){
        this.clientList = clientList;
    }

    public int getClient_id() {
        return client_id;
    }

    public String getName() {
        return name;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }
    public List<Client> getClientList(){return clientList;}
    public void setClientList(List<Client> clientList){this.clientList = clientList;}
    public String toString(){
        return String.format("%-13s%-20s%-13s%n", client_id, name, contact_number);
    }
}
