package entity;

import java.time.LocalDate;

public class Repair {
    private int joborder_id;
    private int technician;
    private LocalDate repair_date;
    private LocalDate end_repair_date;

    public Repair() {

    }

    public Repair(int joborder_id, int technician, String repair_date, String end_repair_date) {
        this.joborder_id = joborder_id;
        this.technician = technician;
        this.repair_date = LocalDate.parse(repair_date);
        this.end_repair_date = LocalDate.parse(end_repair_date);
    }
    public Repair(int joborder_id, int technician, LocalDate repair_date, LocalDate end_repair_date) {
        this.joborder_id = joborder_id;
        this.technician = technician;
        this.repair_date = repair_date;
        this.end_repair_date = end_repair_date;
    }

    public void setJoborder_id(int joborder_id){this.joborder_id = joborder_id;}
    public int getJoborder_id(){return this.joborder_id;}
    public int getTechnician(){return this.technician;}
    public void setTechnician(int technician){this.technician = technician;}
    public void setRepair_date(LocalDate repair_date){ this.repair_date = repair_date;}
    public LocalDate getRepair_date(){return this.repair_date;}
    public LocalDate getEnd_repair_date(){return this.end_repair_date;}
    public void setEnd_repair_date(LocalDate end_repair_date){this.end_repair_date = end_repair_date;}
    public String toString(){

        return String.format("%-20s%-20s%-20s%-13s%n", joborder_id, technician, repair_date, end_repair_date);
    }

}
