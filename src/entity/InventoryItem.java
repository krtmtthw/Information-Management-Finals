package entity;

public class InventoryItem {
    private int part_id;
    private int stock;
    private String part_category;
    private String for_device_type;
    private String brand;
    private String model;
    private double cost_per_piece;

    public InventoryItem() {

    }

    public InventoryItem(int part_id, int stock, String part_category,
                         String for_device_type, String brand, String model,
                         double cost_per_piece) {
        this.part_id = part_id;
        this.stock = stock;
        this.part_category = part_category;
        this.for_device_type = for_device_type;
        this.brand = brand;
        this.model = model;
        this.cost_per_piece = cost_per_piece;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCost_per_piece(double cost_per_piece) {
        this.cost_per_piece = cost_per_piece;
    }

    public void setFor_device_type(String for_device_type) {
        this.for_device_type = for_device_type;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPart_category(String part_category) {
        this.part_category = part_category;
    }

    public void setPart_id(int part_id) {
        this.part_id = part_id;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPart_id() {
        return part_id;
    }

    public double getCost_per_piece() {
        return cost_per_piece;
    }

    public String getBrand() {
        return brand;
    }

    public String getFor_device_type() {
        return for_device_type;
    }

    public String getPart_category() {
        return part_category;
    }

    public int getStock() {
        return stock;
    }

    public String getModel() {
        return model;
    }
}
