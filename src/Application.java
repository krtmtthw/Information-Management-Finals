import dac.ClerkDB;
import dac.LoginDB;
import dac.TechnicianDB;
import entity.*;
import exc.InvalidParametersException;
import exc.InvalidStaffException;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Application {
    Scanner keyboard = new Scanner(System.in);
    ClerkDB clerkDB;
    TechnicianDB technicianDB;
    public String acceptInput(String message) {
        System.out.print(message + ": ");
        return keyboard.nextLine();
    }

    public void login() {
        LoginDB loginDB = new LoginDB();
        String name = acceptInput("Name");
        Staff staff = loginDB.login(name);
        if(staff.getRole() == 1) startTechnicianSession(staff.getStaff_id());
        else startClerkSession(staff.getStaff_id());
    }

    // id will be used for approved_by
    public void startClerkSession(int id) {
        int choice;
        clerkDB = new ClerkDB();
        clerkDB.connect();
        final String menu = """
                1. View Pending Job Orders
                2. Access Job Order Archive
                3. Create a new Job Order
                4. Order Parts
                5. Generate a Receipt
                6. Employee Management
                7. Client Management
                8. Employee Role Management
                9. Exit
                """;
        do {
            System.out.println(menu);
            choice = Integer.parseInt(acceptInput("Input"));
            switch (choice) {
                case 1 -> viewPendingJobOrders(id);
                case 2 -> accessJobOrderArchiveSubMenu();
                case 3 -> createNewJobOrder(id);
                case 4 -> orderPartsSubMenu();
                case 5 -> printFinishedRepairs(id);
                case 6 -> staffSubMenu();
                case 7 -> clientSubMenu();
                case 8 -> employeeRoleSubmenu();
            }
        } while (choice != 9);
    }


    public void accessJobOrderArchiveSubMenu() {
        int choice = 0;
        int jobOrderID = 0;
        Double newValueDouble = 0.00;
        int newValue = 0;
        String clerk = null;
        String approvedString = null;

        do {
            try {
                // header
                System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%n",
                        "Job Order Id",
                        "Client Name",
                        "Approved By",
                        "Request Date",
                        "Approved Date",
                        "Estimated Cost");
                // output list
                for (JobOrder order : clerkDB.getJobOrderArchive()) {
                    if (order.getApproved_by() <= 0) clerk = "N/A";
                    else clerk = clerkDB.getAssociatedStaffName(order.getApproved_by());

                    if (order.getApproved_date() == null) approvedString = "N/A";
                    else approvedString = order.getApproved_date().toString();

                    System.out.printf("%-20d%-20s%-20s%-20tF%-20s%-20.2f%n",
                            order.getJoborder_id(),
                            clerkDB.getAssociatedClientName(order.getClient_id()),
                            clerk,
                            order.getRequest_date(),
                            approvedString,
                            order.getEst_cost()
                    );
                }

                System.out.println("\n1. Modify A Job Order" +
                        "\n2. Go Back");
                choice = Integer.parseInt(acceptInput("Input"));
                if (choice == 1) {
                    modifyJobOrderInterface();
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input.");
            }

        } while (choice != 2);

    }

    public void modifyJobOrderInterface() {
        int choice = 0;
        int jobOrderID = 0;
        Double newValueDouble = 0.00;
        int newValue = 0;
        String clerk = null;
        String approvedString = null;
            try {
                System.out.print("Enter Job Order ID: ");
                jobOrderID = Integer.parseInt(keyboard.nextLine());
                System.out.println("\nEditing JOBORDER #" + jobOrderID +
                        "\n1. Client ID\n" +
                        "2. Approving Staff\n" +
                        "3. Request Date\n" +
                        "4. Approve Date\n" +
                        "5. Estimated Cost\n" +
                        "6. Go Back");
                System.out.print("\nSelect The Field To Be Edited: ");
                choice = Integer.parseInt(keyboard.nextLine());
                if (choice == 1) {
                    System.out.print("Enter The Replacing Client ID: ");
                    int newClientID = Integer.parseInt(keyboard.nextLine());
                    clerkDB.updateJobOrder(jobOrderID, choice, newClientID, newValueDouble, null);
                    return;
                }
                if (choice == 2) {
                    System.out.print("Enter The Replacing ID Of Approving Clerk: ");
                    int approveByID = Integer.parseInt(keyboard.nextLine());
                    clerkDB.updateJobOrder(jobOrderID, choice, approveByID, newValueDouble, null);
                    return;
                }
                if (choice == 3) {
                    LocalDate reqDate = LocalDate.parse(acceptInput("Input date (YYYY-MM-DD)"));
                    clerkDB.updateJobOrder(jobOrderID, choice, newValue, newValueDouble, reqDate);
                    return;
                }
                if (choice == 4) {
                    LocalDate approveDate = LocalDate.parse(acceptInput("Input date (YYYY-MM-DD)"));
                    clerkDB.updateJobOrder(jobOrderID, choice, newValue, newValueDouble, approveDate);
                    return;
                }
                if (choice == 5) {
                    System.out.print("Enter The Estimated Cost: ");
                    double estCost = Double.parseDouble(keyboard.nextLine());
                    clerkDB.updateJobOrder(jobOrderID, choice, newValue, estCost, null);
                    return;
                } if (choice == 6) return;
            } catch (NumberFormatException ee) {
                System.out.println("Invalid input.");
            } catch (DateTimeException dateTimeException) {
                System.out.println("Incorrectly inputted date.");
            }
    }


    public void employeeRoleSubmenu() {
        int choice;
        String menu = """
                1. View list of roles.
                2. Add a role.
                3. Modify a role.
                4. Exit
                """;
        do {
            System.out.println(menu);
            choice = Integer.parseInt(acceptInput("Input"));
            switch (choice) {
                case 1 -> {
                    System.out.println("id \t role");
                    for(Role role : clerkDB.getRoles())
                        System.out.println(role);
                }

                case 2 -> {
                    String roleName = acceptInput("Input role name");
                    Role role = new Role(0, roleName);
                    clerkDB.addRole(role);
                }

                case 3 -> {
                    System.out.println("id\trole");
                    for(Role role : clerkDB.getRoles())
                        System.out.println(role);
                    int chosenIndex = Integer.parseInt(acceptInput("id"));
                    String roleName = acceptInput("Input new role name");
                    Role role = new Role(chosenIndex, roleName);
                    clerkDB.modifyRole(chosenIndex, role);
                }
            }
        } while (choice != 4);
    }
    public void clientSubMenu(){
        List<Client> clientList = new ArrayList<>();
        clientList = clerkDB.getClients();
        int choice = 0;
        String name = "";
        String contact_number = "";
        int client_id = 0;
        do {
            try {
                System.out.println("1. View list of clients" +
                        "\n2. Add a client" +
                        "\n3. Modify a client" +
                        "\n4. Remove a client" +
                        "\n5. Exit");

                choice = Integer.parseInt(acceptInput("Input"));

                switch (choice) {
                    case 1 -> {
                        System.out.printf("%-13s%-20s%-13s%n", "Client ID", "Name", "Contact Number");
                        clientList.stream().forEach(System.out::print);
                        clientList = clerkDB.getClients();
                    }
                    case 2 -> {
                        name = acceptInput("Enter client name");
                        contact_number = acceptInput("Enter client contact number");
                        Client client = new Client(client_id, name, contact_number);
                        clerkDB.addClient(client);
                        clientList = clerkDB.getClients();
                    }
                    case 3 -> {
                        System.out.printf("%-13s%-20s%-13s%n", "client_id", "name", "contact_number");
                        clientList.stream().forEach(System.out::print);
                        client_id = Integer.parseInt(acceptInput("Enter client id"));
                        name = acceptInput("Enter new name");
                        contact_number = acceptInput("Enter new contact number");
                        Client client = new Client(client_id, name, contact_number);
                        clerkDB.modifyClient(client_id, client);
                        clientList = clerkDB.getClients();
                    }
                    case 4 -> {
                        System.out.printf("%-13s%-20s%-13s%n", "client_id", "name", "contact_number");
                        clientList.stream().forEach(System.out::print);
                        client_id = Integer.parseInt(acceptInput("Enter client id"));
                        clerkDB.removeClient(client_id);
                        clientList = clerkDB.getClients();
                    }
                }
            } catch (Exception e) {
                System.out.println("Invalid input.");
            }
        } while (choice != 5);
    }
    public void orderPartsSubMenu(){
        System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%-20s%n",
                "Part Id",
                "Stock",
                "Part Category",
                "For Device Type",
                "Brand",
                "Model",
                "Cost Per Piece");
        for (InventoryItem items : clerkDB.displayInventory()) {
            String model = items.getModel();
            if (model == null) {
                model = "N/A";
            }
            System.out.printf("%-20d%-20d%-20s%-20s%-20s%-20s%-20.2f%n",
                    items.getPart_id(),
                    items.getStock(),
                    items.getPart_category(),
                    items.getFor_device_type(),
                    items.getBrand(),
                    model,
                    items.getCost_per_piece());
        }
        int choice = 0;
        do {
            System.out.println("\n1. Create new parts order" +
                    "\n2. Mark Parts as received" +
                    "\n3. Exit");
            choice = Integer.parseInt(acceptInput("Input"));
            if (choice == 1) {
                orderParts();
            } else if (choice == 2){
                receiveParts();
            }
        } while (choice != 3);
    }
    public void orderParts(){
        LocalDate orderDate = null;
        int partID = Integer.parseInt(acceptInput("Enter Part Id to be ordered"));
        do {
            try {
                orderDate = LocalDate.parse(acceptInput("Order date (YYYY-MM-DD)"));
            } catch (Exception e){
                System.out.println("Invalid Input");
                continue;
            }
            break;
        } while (true);
        clerkDB.orderParts(orderDate);
        System.out.println();
        System.out.println("Database updated successfully");
        System.out.println("Press Enter to continue");
        keyboard.nextLine();
    }
    public void receiveParts(){
        System.out.printf("%-20s%-20s%-20s%n", "Parts Order ID", "Job Order ID", "Order Date");
        for(PartsOrder orderParts : clerkDB.pendingPartsOrder()){
            System.out.printf("%-20d%-20d%-20tF%n",
                    orderParts.getPartsorder_id(),
                    orderParts.getJoborder_id(),
                    orderParts.getOrder_date());
        }
        System.out.println();
        int partID = Integer.parseInt(acceptInput("Enter Part Id that was received"));
        int quantity =Integer.parseInt(acceptInput("Enter quantity arrived"));
        LocalDate arriveDate = LocalDate.parse(acceptInput("Arrived Date (YYYY-MM-DD)"));
        clerkDB.updateInventory(partID,quantity,1);
        clerkDB.updateArriveDate(partID,arriveDate);
        System.out.println();
        System.out.println("Database updated successfully");
        System.out.println("Press Enter to continue");
        keyboard.nextLine();
    }
    public void staffSubMenu() {
        List<Staff> staffList = new ArrayList<>();
        staffList = clerkDB.getStaff();
        int choice = 0;
        int staff_id = 0;
        String name = "";
        String termination_date = "";
        int role_id = 0;
        do {
            try {
                System.out.println("1. View list of staffs/employees" +
                        "\n2. Add a staff" +
                        "\n3. Modify a staff" +
                        "\n4. Remove a staff" +
                        "\n5. Exit");
                choice = Integer.parseInt(acceptInput("Input"));

                switch (choice) {
                    case 1 -> {
                        System.out.printf("%-13s%-13s%-20s%-15s%-15s%n", "Staff ID", "Role_ID", "Name", "Hire_Date", "Termination_Date");
                        for (Staff staff : clerkDB.getStaff()) {
                            System.out.print(staff);
                        }
                    }
                    case 2 -> {
                        String staffName = acceptInput("Employee name");
                        int role = Integer.parseInt(acceptInput("role (-1 if no role)"));
                        LocalDate hire_date = LocalDate.parse(acceptInput("Hire date (YYYY-MM-DD)"));
                        Staff staff = new Staff(-1, staffName, role, hire_date);
                        clerkDB.addStaff(staff);
                    }
                    case 3 -> {
                        System.out.printf("%-13s%-13s%-20s%-15s%-15s%n", "Staff ID", "Role_ID", "Name", "Hire_Date", "Termination_Date");
                        for (Staff staff : clerkDB.getStaff()) {
                            System.out.print(staff);
                        }
                        int chosenIndex = Integer.parseInt(acceptInput("id"));
                        String staffName = acceptInput("Employee name");
                        int role = Integer.parseInt(acceptInput("role (-1 if no role)"));
                        LocalDate hire_date = LocalDate.parse(acceptInput("Hire date (YYYY-MM-DD)"));
                        Staff staff = new Staff(-1, staffName, role, hire_date);
                        clerkDB.modifyStaff(chosenIndex, staff);
                    }
                    case 4 -> {
                        System.out.printf("%-13s%-13s%-20s%-15s%-15s%n", "Staff ID", "Role_ID", "Name", "Hire_Date", "Termination_Date");
                        staffList.stream().forEach(System.out::print);
                        staff_id = Integer.parseInt(acceptInput("Enter staff id"));
                        clerkDB.removeStaff(staff_id);
                    }
                    case 5 -> {
                        return;
                    }
                    default -> {
                        throw new InvalidParametersException("");
                    }
                }
            } catch (NumberFormatException | InvalidParametersException exception) {
                System.err.println("Invalid input.");
            }
        } while (choice != 5);
    }
    public void startTechnicianSession(int id) {
        int choice;
        technicianDB = new TechnicianDB();
        technicianDB.connect();

        final String menu = "1. View Conducted Repair Archive " +
                "\n2. View Assigned Ongoing Repairs " +
                "\n3. Mark Repair as Finished" +
                "\n4. Back";
        do {
            System.out.println(menu);
            choice = Integer.parseInt(acceptInput("Input"));

            switch (choice){

                case 1 ->  {
                    viewConductedRepairs(id);
                }
                case 2 -> {
                    viewAssignedOngoingRepairs(id);
                }

                case 3 -> {
                    markRepairAsFinished(id);
                }
                default ->{
                    continue;
                }
            }

        }while(choice != 4);

    }
    public void viewConductedRepairs(int technician_id){
        List<Repair> repairList = new ArrayList<>();
        System.out.printf("%-20s%-20s%-20s%-13s%n", "Job Order ID","Technician ID","Repair Date","End Repair Date");
        repairList = technicianDB.showRepairArchive(technician_id);
        repairList.stream().forEach(System.out::print);

    }
    public void viewPendingJobOrders(int id){
        System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%n",
                "Job Order Id",
                "Client Name",
                "Approved By",
                "Request Date",
                "Approved Date",
                "Estimated Cost");

        for(JobOrder jobOrder : clerkDB.getPendingJobOrders()){
            String clerk = "";
            if(jobOrder.getApproved_by() <= 0) {
                clerk = "N/A";
            }
            if (jobOrder.getApproved_date() != null) {
                //Print format follows int, string, string, date (YYYY-MM-DD), date (YYYY-MM-DD), float w/ 2 decimal places
                System.out.printf("%-20d%-20s%-20s%-20tF%-20tF%-20.2f%n",
                        jobOrder.getJoborder_id(),
                        clerkDB.getAssociatedClientName(jobOrder.getClient_id()),
                        clerk,
                        jobOrder.getRequest_date(),
                        jobOrder.getApproved_date(),
                        jobOrder.getEst_cost());
            } else {
                //Print format follows int, string, string, date (YYYY-MM-DD), string for "N/A", float w/ 2 decimal places
                System.out.printf("%-20d%-20s%-20s%-20tF%-20s%-20.2f%n",
                        jobOrder.getJoborder_id(),
                        clerkDB.getAssociatedClientName(jobOrder.getClient_id()),
                        clerk,
                        jobOrder.getRequest_date(),
                        "N/A",
                        jobOrder.getEst_cost());
            }

        }
        System.out.println();
        pendingJobOrderSubmenu(id);
    }

    public void pendingJobOrderSubmenu(int id){
        int choice;
        String menu = """
                1. Select Job Order Id
                2. Exit
                """;
        String action = """
                Input letter of action
                1. approve(a)
                2. reject(r) 
                """;

        System.out.println(menu);
        choice = Integer.parseInt(acceptInput("Input"));
        if (choice == 1) {
            int jobID = Integer.parseInt(acceptInput("Input Job Order ID"));
            System.out.println();
            System.out.println("Selected Job Order Id " + jobID);
            System.out.println(action);
            char operation = acceptInput("Input action").charAt(0);
            clerkDB.processJobOrders(operation, jobID, id);
            System.out.print("Press enter to continue...");
            keyboard.nextLine();
        }
    }

    //This method associates items to a job order
    public double associateItems(int jobOrderID){
        double estcost = 0.0;
        char choice = 'y';
        while(choice == 'y') {
            int partID = Integer.parseInt(acceptInput("Input part id"));
            int quantity = Integer.parseInt(acceptInput("Quantity"));
            for(InventoryItem items : clerkDB.displayInventory()){
                if(items.getPart_id() == partID) {
                    if (items.getStock() < quantity) {
                        System.out.println("Quantity entered is greater than quantity in stocks. Please Order parts for Part ID: " + partID);
                    } else {
                        estcost += quantity * clerkDB.getCostPerPiece(partID);
                        clerkDB.updateInventory(partID, quantity,2);
                        break;
                    }
                }
            }
            choice = acceptInput("Add more? (y/n)").charAt(0);
        }
        return estcost;
    }
    //This method allows the creation of new job orders
    public void createNewJobOrder(int id) {
        char action = '0';
        char option = '0';
        double estCost = 0.0;
        int client_id = 0;
        int newJobOrderID = clerkDB.getNewJobOrderID();
        LocalDate reqDate;
        System.out.println("New Job Order ID: " + newJobOrderID);
            // show list of clients
            for (Client client : clerkDB.getClients()) System.out.print(client);
            do {
                try {
                    client_id = Integer.parseInt(acceptInput("Input Client's ID"));
                } catch (Exception e) {
                    System.out.println("Invalid input.");
                    continue;
                }
                break;
            } while (true);
            // String clientName = clerkDB.getAssociatedClientName(client_id);
        do {
            try {
                reqDate = LocalDate.parse(acceptInput("Request date (YYYY-MM-DD)"));
                if (reqDate != null) break;
            } catch (Exception e) {
                System.out.println("Invalid input.");
                continue;
            }
            break;
        } while (true);
            do {
                try {
                    action = acceptInput("Associate Items(y/n)").charAt(0);
                    if (action == 'y' || action == 'n') break;
                    throw new InvalidParametersException("");
                } catch (Exception e) {
                    System.out.println("Invalid input.");
                    continue;
                }
            } while (true);

            if (action == 'y') {
                System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%-20s%n",
                        "Part Id",
                        "Stock",
                        "Part Category",
                        "For Device Type",
                        "Brand",
                        "Model",
                        "Cost Per Piece");
                for (InventoryItem items : clerkDB.displayInventory()) {
                    String model = items.getModel();
                    if (model == null) {
                        model = "N/A";
                    }
                    System.out.printf("%-20d%-20d%-20s%-20s%-20s%-20s%-20.2f%n",
                            items.getPart_id(),
                            items.getStock(),
                            items.getPart_category(),
                            items.getFor_device_type(),
                            items.getBrand(),
                            model,
                            items.getCost_per_piece());
                }
                System.out.println();
                estCost = associateItems(newJobOrderID);
            }
            System.out.println();
            String processes = """
                    What to do with current job order?
                    1. accept(a)
                    2. reject(r)
                    3. withhold(w)
                    """;
            do {
                try {
                    System.out.println(processes);
                    option = acceptInput("Input choice").charAt(0);
                    if (option == 'a' || option == 'r' || option == 'w') break;
                    throw new InvalidParametersException("");
                } catch (Exception e) {
                    System.out.println("Invalid input");
                    continue;
                }
            } while (true);
            clerkDB.createNewJobOrder(id, newJobOrderID, client_id, reqDate, estCost, option);
            System.out.println();
            System.out.println("Press enter to continue...");
            keyboard.nextLine();
    }
    //This method prints all the finished repairs that has not been paid yet
    public void printFinishedRepairs(int clerkID){
        Double totalCost = 0.0;
        System.out.printf("%-20s%-20s%-20s%-20s%-20s%n",
                "Job Order Id",
                "Client Name",
                "Repair Date",
                "End Repair Date",
                "Total Cost");
        for(Repair finRepair : clerkDB.getFinishedJobOrders()){
            System.out.printf("%-20d%-20s%-20tF%-20tF%-20.2f%n",
                    finRepair.getJoborder_id(),
                    clerkDB.getAssociatedClientName(clerkDB.getClientID(finRepair.getJoborder_id())),
                    finRepair.getRepair_date(),
                    finRepair.getEnd_repair_date(),
                    clerkDB.getTotalCost(finRepair.getJoborder_id()));
            totalCost = clerkDB.getTotalCost(finRepair.getJoborder_id());
        }
        System.out.println();
        generateReceiptMenu(clerkID,totalCost);

    }
    //This method allows the clerk to generate a receipt for a certain Job order Id
    public void generateReceiptMenu(int clerkID, double totalCost){
        int choice;
        String menu = """
                1. Select Job Order Id
                2. Exit
                """;
        System.out.println(menu);
        choice = Integer.parseInt(acceptInput("Input"));
        switch(choice) {
            case 1 -> {
                int jobID = Integer.parseInt(acceptInput("Input Job Order ID"));
                System.out.println();
                System.out.println("Selected Job Order Id " + jobID);
                LocalDate payDate = LocalDate.parse(acceptInput("Payment date (YYYY-MM-DD)"));
                clerkDB.updatePayDate(clerkID,jobID,payDate,totalCost);
                System.out.println();
                System.out.println("Database updated successfully.");
                System.out.print("Press enter to continue...");
                keyboard.nextLine();
                System.out.println();
                printReceipt(jobID);
            }
        }
    }
    //This method prints the details of the receipt
    public void printReceipt(int jobOrderID){
        Receipt receipt = clerkDB.getReceiptDetails(jobOrderID);
        Repair repair = clerkDB.getRepairDetails(jobOrderID);
        System.out.printf("%-20s%n","Receipt Details:");
        System.out.printf("%-20s%-20s%n","Job Order: ",jobOrderID);
        System.out.printf("%-20s%-20s%n","Client Name: ",clerkDB.getAssociatedClientName(clerkDB.getClientID(jobOrderID)));
        System.out.printf("%-20s%-20s%n","Payment Processor: ", clerkDB.getStaffName(receipt.getPayment_processor()));
        System.out.printf("%-20s%-20s%n","Technician: ", clerkDB.getStaffName(clerkDB.getTechStaffID(jobOrderID)));
        System.out.printf("%-20s%-20tF%n","Repair Date: ",repair.getRepair_date());
        System.out.printf("%-20s%-20tF%n","End Repair Date: ",repair.getEnd_repair_date());
        System.out.printf("%-20s%-20tF%n","Payment Date: ",receipt.getPay_date());
        System.out.printf("%-20s%-20.2f%n","Total Cost: ", receipt.getFin_cost());
        System.out.println();
        System.out.print("Press enter to continue...");
        keyboard.nextLine();
    }
    public void createPartsOrder(int jobOrderID) {
        System.out.println("Ordering parts for Job order: " + jobOrderID);
        LocalDate orderDate = null;
        do {
            try {
                orderDate = LocalDate.parse(acceptInput("Order date (YYYY-MM-DD)"));
            } catch (Exception e){
                System.out.println("Invalid Input");
                continue;
            }
            break;
        } while (true);
        clerkDB.setNewPartsOrder(jobOrderID, orderDate);
    }

    public void viewAssignedOngoingRepairs(int technician_id) {
        clerkDB = new ClerkDB();
        clerkDB.connect();
        String start_repair_string, end_repair_string;
        for(Repair repair: technicianDB.showAssignedOngoingRepairs(technician_id)) {
            if (repair.getRepair_date() == null) start_repair_string = "N/A";
            else start_repair_string = repair.getRepair_date().toString();
            if (repair.getEnd_repair_date() == null) end_repair_string = "N/A";
            else end_repair_string = repair.getEnd_repair_date().toString();
            System.out.printf("%-20s%-20s%-20s%-20s%-13s%n", "Job Order ID","Technician", "Client",
                    "Repair Date","End Repair Date");
            System.out.printf("%-20s%-20s%-20s%-20s%-13s%n",
                    repair.getJoborder_id(),
                    clerkDB.getAssociatedStaffName(repair.getTechnician()),
                    clerkDB.getAssociatedClientName(clerkDB.getClientID(repair.getJoborder_id())),
                    start_repair_string,
                    end_repair_string
            );
        }
    }

    public void markRepairAsFinished(int technician_id) {
        clerkDB = new ClerkDB();
        clerkDB.connect();
        int jobOrderID = -1;
        LocalDate end_repair_date;
        String start_repair_string, end_repair_string;
        System.out.printf("%-20s%-20s%-20s%-20s%-13s%n", "Job Order ID","Technician", "Client",
                "Repair Date","End Repair Date");
        for (Repair repair : technicianDB.showAssignedOngoingRepairs(technician_id)) {
            if (repair.getRepair_date() == null) start_repair_string = "N/A";
            else start_repair_string = repair.getRepair_date().toString();
            if (repair.getEnd_repair_date() == null) end_repair_string = "N/A";
            else end_repair_string = repair.getEnd_repair_date().toString();
            System.out.printf("%-20s%-20s%-20s%-20s%-13s%n",
                    repair.getJoborder_id(),
                    clerkDB.getAssociatedStaffName(repair.getTechnician()),
                    clerkDB.getAssociatedClientName(clerkDB.getClientID(repair.getJoborder_id())),
                    start_repair_string,
                    end_repair_string
            );
        }
        jobOrderID = Integer.parseInt(acceptInput("Input job order id"));
        end_repair_date = LocalDate.parse(acceptInput("Input completion date (YYYY-MM-DD)"));
        technicianDB.markRepairAsFinished(jobOrderID, end_repair_date);
    }

    public static void main(String[] args) {
        try {
            Application application = new Application();
            application.login();
        } catch (InvalidStaffException e) {
            System.out.println(e.getMessage());
        }

    }
}
