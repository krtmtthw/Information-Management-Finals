package exc;

public class InventoryFailureException extends RuntimeException {
    public InventoryFailureException(String message) {
        super(message);
    }
}
