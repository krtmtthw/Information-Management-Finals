package exc;

public class InvalidStaffException extends RuntimeException {
    public InvalidStaffException(String message) {
        super(message);
    }
}
