package unused;

public class PartsUsed {
    private int joborder_id;
    private int part_id;
    private int quantity_used;

    public PartsUsed() {

    }

    public PartsUsed(int joborder_id, int part_id, int quantity_used) {
        this.joborder_id = joborder_id;
        this.part_id = part_id;
        this.quantity_used = quantity_used;
    }

    public int getJoborder_id() {
        return joborder_id;
    }

    public int getPart_id() {
        return part_id;
    }

    public int getQuantity_used() {
        return quantity_used;
    }

    public void setJoborder_id(int joborder_id) {
        this.joborder_id = joborder_id;
    }

    public void setPart_id(int part_id) {
        this.part_id = part_id;
    }

    public void setQuantity_used(int quantity_used) {
        this.quantity_used = quantity_used;
    }
}
