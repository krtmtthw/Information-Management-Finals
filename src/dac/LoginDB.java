package dac;

import entity.Staff;
import exc.InvalidStaffException;

import java.sql.*;

public class LoginDB {
    private Connection connection;
    private String dbConfig;

    public LoginDB() {
        dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
                + "company" + "?"
                + "user=" + "root" + "&"
                + "password=";
    }

    public Staff login(String name) {
        Staff staff = null;
        String queryString;
        ResultSet resultSet;
        PreparedStatement preparedStatement;

        try {
            connection = DriverManager.getConnection(dbConfig);
            queryString = "SELECT * FROM company.staff WHERE name = ?" +
                    " AND termination_date IS NULL";
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                staff = new Staff(resultSet.getInt(1),
                        resultSet.getString(2), resultSet.getInt(3));
                return staff;
            }

            throw new InvalidStaffException("No such staff or staff was recently terminated.");
        } catch (InvalidStaffException | NullPointerException exception) {
            System.err.println(exception.getMessage());
            System.exit(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
