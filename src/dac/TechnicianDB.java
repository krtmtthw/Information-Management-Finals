package dac;

import entity.*;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TechnicianDB {
    private Connection connection;
    private String dbConfig;
    public TechnicianDB() {
        dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
                + "company" + "?"
                + "user=" + "root" + "&"
                + "password=";
    }

    public Connection connect() {
        try {
            connection = DriverManager.getConnection(dbConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public List<Repair> showRepairArchive(int technician_id){
        List<Repair> repairList = new ArrayList<>();
        String queryString = "SELECT * FROM company.repair WHERE technician="+technician_id+" ORDER BY 1";
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);

            while (resultSet.next()) {
                if(resultSet.getString(3) != null && resultSet.getString(4) != null) {
                    repairList.add(new Repair(resultSet.getInt(1), resultSet.getInt(2), resultSet.getString(3), resultSet.getString(4)));
                }
                resultSet.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return repairList;
    }

    public List<Repair> showAssignedOngoingRepairs(int technician_id) {
        List<Repair> assignedJobOrders = new ArrayList<>();
        PreparedStatement preparedStatement;
        String queryString = "SELECT * FROM company.repair WHERE technician = ?" +
                " AND end_repair_date IS NULL";
        ResultSet resultSet;
        java.sql.Date start_date = null;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, technician_id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getObject(3) != null)
                    start_date = resultSet.getDate(3);
                assignedJobOrders.add(new Repair(resultSet.getInt(1),
                        resultSet.getInt(2),
                        start_date.toLocalDate(),
                        null));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return assignedJobOrders;
    }
    public void markRepairAsFinished(int joborder_id, LocalDate end_repair_date) {
        PreparedStatement preparedStatement;
        String queryString = "UPDATE company.repair SET end_repair_date = ? " +
                "WHERE joborder_id = ?";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setDate(1, java.sql.Date.valueOf(end_repair_date));
            preparedStatement.setInt(2, joborder_id);

            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            System.out.println("Invalid arguments or job order does not exist.");
        }
    }
}