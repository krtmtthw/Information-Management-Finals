package dac;

import entity.*;
import exc.InvalidParametersException;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

// DO NOT USE THE STATIC KEYWORD
// WE ARE ALLOWED TO MAKE CHANGES
// TO THE CODE ARCHITECTURE
public class ClerkDB {
    private Connection connection;
    private String dbConfig;

    public ClerkDB() {
        dbConfig = "jdbc:mariadb://127.0.0.1:3306" + "/"
                + "company" + "?"
                + "user=" + "root" + "&"
                + "password=";
    }

    public Connection connect() {
        try {
            connection = DriverManager.getConnection(dbConfig);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    // for base table: role
    // handle exceptions locally
    // DO NOT USE A 'THROWS' CLAUSE
    public List<Role> getRoles() { // no 'throws Exception'
        List<Role> roleList = new ArrayList<>();
        String queryString = "SELECT * FROM company.role ORDER BY 1";
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);

            while (resultSet.next()) {
                Role role = new Role(resultSet.getInt(1), resultSet.getString(2));
                roleList.add(role);
                resultSet.close();
            }
        } catch (SQLException e) {
            // exceptions are handled locally
            e.printStackTrace();
        }

        return roleList;
    }

    public void addRole(Role role) {
        PreparedStatement preparedStatement;
        // USE A NULL IF THE FIELD IS AN INT ID FIELD
        // BECAUSE THE DBMS USES AN AUTOINCREMENT SYSTEM
        String maxString = "SELECT MAX(role_id) FROM company.role";
        Statement maxStatement;
        ResultSet resultSet;
        int maxInt = -1;
        String queryString = "INSERT INTO company.role(role_id, role_name) " +
                "VALUES(?, ?)";
        try {
            // get max value
            maxStatement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = maxStatement.executeQuery(maxString);

            while (resultSet.next()) maxInt = resultSet.getInt(1);
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, maxInt + 1);
            preparedStatement.setString(2, role.getRole_name());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<JobOrder> getJobOrderArchive() {
        List<JobOrder> jobOrderList = new ArrayList<>();
        ResultSet resultSet;
        Statement query;
        String queryString = "SELECT * FROM company.joborder";
        try {
            query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            while (resultSet.next()) {
                jobOrderList.add(new JobOrder(resultSet.getInt(1), resultSet.getInt(2),
                        resultSet.getInt(3),
                        resultSet.getDate(4),
                        resultSet.getDate(5),
                        resultSet.getDouble(6)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobOrderList;
    }

    // update operation
    public void modifyRole(int oldRoleIndex, Role newRole) throws InvalidParametersException {
        PreparedStatement preparedStatement;
        String queryString = "UPDATE company.role SET role_id = ?, role_name = ? WHERE role_id = ?";
        ResultSet resultSet;
        try {
            // Return a resultset of one item
            // because you are querying by id
            // ids guarantee a unique instance
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, oldRoleIndex);
            preparedStatement.setString(2, newRole.getRole_name());
            preparedStatement.setInt(3, oldRoleIndex);

            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Client> getClients() {
        List<Client> clientList = new ArrayList<>();
        String queryString = "SELECT * FROM company.client ORDER BY 1";
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);

            while (resultSet.next()) {
                clientList.add(new Client(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3)));
                resultSet.close();
            }
        } catch (SQLException e) {
            // exceptions are handled locally
            e.printStackTrace();
        }

        return clientList;
    }

    public void addClient(Client client) {
        PreparedStatement preparedStatement;
        String queryString = "INSERT INTO company.client(client_id, name, contact_number) " +
                "VALUES(?, ?, ?)";
        int newID = -1;
        try {
            newID = getNewClientID();
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, newID);
            preparedStatement.setString(2, client.getName());
            preparedStatement.setString(3, client.getContact_number());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modifyClient(int client_id, Client client) {
        PreparedStatement preparedStatement;
        String queryString = "UPDATE company.client SET name = ?, contact_number = ? WHERE client_id = ?";

        try {

            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, client.getName());
            preparedStatement.setString(2, client.getContact_number());
            preparedStatement.setInt(3, client_id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeClient(int client_id) {
        PreparedStatement preparedStatement;
        String queryString = "DELETE FROM company.client WHERE client_id=?";

        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, client_id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Invalid program arguments or client has existing job orders.");
        }
    }

    public List<Staff> getStaff() {
        List<Staff> staffList = new ArrayList<>();
        String queryString = "SELECT * FROM company.staff ORDER BY 1";
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);

            while (resultSet.next()) {
                if (resultSet.getString(4) != null && resultSet.getString(5) != null) {
                    staffList.add(new Staff(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5)));
                } else if (resultSet.getString(4) == null && resultSet.getString(5) != null) {
                    staffList.add(new Staff(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), null, resultSet.getString(5)));
                } else if (resultSet.getString(4) != null && resultSet.getString(5) == null) {
                    staffList.add(new Staff(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), null));
                } else if (resultSet.getString(4) == null && resultSet.getString(5) == null) {
                    staffList.add(new Staff(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), null, null));
                }
                resultSet.close();
            }
        } catch (SQLException e) {
            // exceptions are handled locally
            e.printStackTrace();
        }

        return staffList;
    }

    public void addStaff(Staff staff) {
        String maxString = "SELECT MAX(staff_id) FROM company.staff";
        Statement maxStatement;
        ResultSet resultSet;
        int maxInt = -1;
        PreparedStatement preparedStatement;
        String queryString = "INSERT INTO company.staff(staff_id, name, role, hire_date, termination_date) " +
                "VALUES(?, ?, ?, ?, NULL);";
        try {
            maxStatement = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = maxStatement.executeQuery(maxString);

            while (resultSet.next()) maxInt = resultSet.getInt(1);
            preparedStatement = connection.prepareStatement(queryString);

            preparedStatement.setInt(1, maxInt + 1);
            preparedStatement.setString(2, staff.getName());
            // test if a staff has a role or not
            // case -1 if staff has no role
            if(staff.getRole() == -1) {
                preparedStatement.setNull(3, Types.INTEGER);
            }  else {
                preparedStatement.setInt(3, staff.getRole());
            }
            preparedStatement.setDate(4,
                    java.sql.Date.valueOf(staff.getHire_date()));
            preparedStatement.execute();

            preparedStatement.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifyStaff(int oldEmployeeIndex, Staff newStaff) {
        PreparedStatement preparedStatement;
        String queryString = "UPDATE company.staff SET name = ?, role = ?, hire_date = ?, termination_date = ? " +
                "WHERE staff_id = ?";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, newStaff.getName());

            // test for no role
            if (newStaff.getRole() == -1)
                preparedStatement.setNull(2, Types.INTEGER);
            else
                preparedStatement.setInt(2, newStaff.getRole());

            preparedStatement.setDate(3, java.sql.Date.valueOf(newStaff.getHire_date()));

            if (newStaff.getTermination_date() == null)
                preparedStatement.setNull(4, Types.DATE);
            else preparedStatement.setDate(4, java.sql.Date.valueOf(newStaff.getHire_date()));
            preparedStatement.setInt(5, oldEmployeeIndex);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeStaff(int staff_id) {
        PreparedStatement preparedStatement;
        String queryString = "DELETE FROM  company.staff WHERE staff_id=?";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, staff_id);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            System.err.println("Invalid program arguments or staff has associated job orders.");
        }
    }

    //This method returns a list of job orders that have not yet been approved
    public List<JobOrder> getPendingJobOrders() {
        List<JobOrder> jobOrderList = new ArrayList<JobOrder>();
        String queryString = "SELECT * FROM company.joborder WHERE approved_date IS NULL";//query that gathers all pending job orders
        // Or job orders that have not yet been approved
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            resultSet.beforeFirst();
            while (resultSet.next()) {

                JobOrder jobOrder = new JobOrder(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3),
                        resultSet.getDate(4),
                        resultSet.getDate(5),
                        resultSet.getInt(6));
                jobOrderList.add(jobOrder);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jobOrderList;
    }

    //This method is responsible for processing job orders (approve, reject, or withheld)
    public void processJobOrders(char choice, int jobOrderID, int clerkID) {
        String queryString;
        PreparedStatement preparedStatement;
        try {
            switch (choice) {
                case 'a' -> {
                    queryString = "UPDATE company.joborder SET approved_by = ?, approved_date = ? WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setInt(1, clerkID);
                    preparedStatement.setDate(2, java.sql.Date.valueOf(java.time.LocalDate.now()));
                    preparedStatement.setInt(3, jobOrderID);

                    preparedStatement.executeUpdate();
                    preparedStatement.close();

                }
                case 'r' -> {
                    queryString = "DELETE FROM company.joborder WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setInt(1, jobOrderID);

                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                case 'w' -> {
                    queryString = "UPDATE company.joborder SET approved_by = ?, approved_date = ? WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setNull(1, Types.INTEGER);
                    preparedStatement.setDate(2, null);
                    preparedStatement.setInt(3, jobOrderID);

                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
            }
            System.out.println("Database updated successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getAssociatedStaffName(int technicianID) throws InvalidParametersException {
        String queryString = "SELECT name FROM company.staff WHERE staff_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet = null;
        String name = "";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, technicianID);
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.isBeforeFirst()) throw new InvalidParametersException("No such technician.");

            while (resultSet.next()) {
                name = resultSet.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return name;
    }

    //This method returns the name of the client associated with the client ID
    public String getAssociatedClientName(int clientId) {
        String queryString = "SELECT name FROM company.client WHERE client_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String name = "";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, clientId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                name = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }

    //This method acquires the client ID from the database if the client exists
    public int getAssociatedClientID(String clientName) {
        String queryString = "SELECT client_id FROM company.client WHERE name = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int clientID = getNewClientID();//Invoke getNewClientID() method to generate new ID
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setString(1, clientName);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                clientID = resultSet.getInt(1);//Overwrite client ID if a client exists in the database
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clientID;
    }

    //This method creates a new Client Id if client does not exist in clients database
    public int getNewClientID() {
        String queryString = "SELECT MAX(client_id) FROM company.client";

        ResultSet resultSet;
        int newClientID = -1;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            while (resultSet.next()) {
                newClientID = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newClientID + 1;
    }

    //This method generates a new job order ID for new job orders
    public int getNewJobOrderID() {
        String queryString = "SELECT MAX(joborder_id) FROM company.joborder";
        ResultSet resultSet;
        int newJobOrderID = -1;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            while (resultSet.next()) {
                newJobOrderID = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newJobOrderID + 1;
    }

    //This method displays the different items in the inventory and their details
    public List<InventoryItem> displayInventory() {
        List<InventoryItem> inventoryItemList = new ArrayList<InventoryItem>();
        String queryString = "SELECT i.part_id, i.stock, pc.cat_name, pd.type_name, pb.brand_name, i.model, i.cost_per_piece " +
                "FROM company.inventory i\n" +
                "JOIN company.part_brand pb\n" +
                "ON i.brand = pb.brand_id\n" +
                "JOIN company.part_category pc\n" +
                "ON pc.cat_id = i.part_category\n" +
                "JOIN company.part_devicetype pd\n" +
                "ON i.for_device_type = pd.type_id\n" +
                "ORDER BY 1;";

        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            resultSet.beforeFirst();
            while (resultSet.next()) {
                InventoryItem inventory = new InventoryItem(resultSet.getInt(1),
                        resultSet.getInt(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getDouble(7));
                inventoryItemList.add(inventory);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inventoryItemList;
    }

    //This method retrieves the cost per piece associated with the item part
    public double getCostPerPiece(int partID) {
        String queryString = "SELECT cost_per_piece FROM company.inventory WHERE part_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        double pieceCost = 0.0;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setDouble(1, partID);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                pieceCost = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pieceCost;
    }

    //This method will insert the new job order in the joborder database
    public void createNewJobOrder(int clerkid, int jobOrderId, int clientId, LocalDate reqDate, double estcost, char option) {
        String queryString = "INSERT INTO company.joborder(joborder_id,client_id,request_date,est_cost) VALUES(?,?,?,?)";
        PreparedStatement preparedStatement;
        try {
            if (option == 'a' || option == 'w') {
                preparedStatement = connection.prepareStatement(queryString);
                preparedStatement.setInt(1, jobOrderId);
                preparedStatement.setInt(2, clientId);
                preparedStatement.setDate(3, java.sql.Date.valueOf(reqDate));
                preparedStatement.setDouble(4, estcost);
                preparedStatement.executeUpdate();
                preparedStatement.close();
                processJobOrders(option, jobOrderId, clerkid);

            } else {
                System.out.println("Job Order Rejected...");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void orderParts(LocalDate orderDate) {
        String queryString = "INSERT INTO company.partsOrder(partsorder_id, joborder_id, order_date, arrive_date)" +
                "VALUES(?,NULL,?,NULL)";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1,getNewPartsOrderID());
            preparedStatement.setDate(2,java.sql.Date.valueOf(orderDate));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateJobOrder(int jobID, int choice, int newValue, Double newValueDouble, LocalDate newDate) {
        PreparedStatement preparedStatement;
        String queryString;
        try {
            switch (choice) {
                case 1:
                    queryString = "UPDATE company.joborder SET client_id = ?  WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setInt(1, newValue);
                    preparedStatement.setInt(2, jobID);
                    preparedStatement.executeUpdate();
                    break;

                case 2:
                    queryString = "UPDATE company.joborder SET approved_by = ?  WHERE joborder_id =?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setInt(1, newValue);
                    preparedStatement.setInt(2, jobID);
                    preparedStatement.executeUpdate();
                    break;

                case 3:
                    queryString = "UPDATE company.joborder SET request_date = ?  WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setDate(1, java.sql.Date.valueOf(newDate));
                    preparedStatement.setInt(2, jobID);
                    preparedStatement.executeUpdate();
                    break;
                case 4:
                    queryString = "UPDATE company.joborder SET approved_date = ?  WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setDate(1, java.sql.Date.valueOf(newDate));
                    preparedStatement.setInt(2, jobID);
                    preparedStatement.executeUpdate();
                    break;

                case 5:
                    queryString = "UPDATE company.joborder SET est_cost = ?  WHERE joborder_id = ?";
                    preparedStatement = connection.prepareStatement(queryString);
                    preparedStatement.setDouble(1, newValueDouble);
                    preparedStatement.setInt(2, jobID);
                    preparedStatement.executeUpdate();
                    break;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //This method returns a list of finished job orders that have not been paid yet
    public List<Repair> getFinishedJobOrders(){
        List<Repair> finishedRepairsList = new ArrayList<Repair>();
        String queryString = "SELECT rp.joborder_id, rp.technician, rp.repair_date, rp.end_repair_date\n" +
                "FROM company.repair rp JOIN company.receipt rc \n" +
                "ON rp.joborder_id = rc.joborder_id\n" +
                "WHERE rp.end_repair_date IS NOT NULL AND rc.pay_date IS NULL";

        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            resultSet.beforeFirst();
            while (resultSet.next()) {

                Repair finishedRepairs = new Repair(resultSet.getInt(1), resultSet.getInt(2),
                        String.valueOf(resultSet.getDate(3)), String.valueOf(resultSet.getDate(4)));
                finishedRepairsList.add(finishedRepairs);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return finishedRepairsList;
    }
    //This method returns the totalcost of a repair
    public double getTotalCost(int jobOrderID){
        String queryString = "SELECT fin_cost FROM company.receipt WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        double totalCost = 0.0;
        try {

            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setDouble(1,jobOrderID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                totalCost = resultSet.getDouble(1);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalCost;
    }
    //This method acquires the client ID from the database that matches a certain job order id
    public int getClientID(int jobOrderID){
        String queryString = "SELECT client_id FROM company.joborder WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int clientID = 0;
        try {

            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setDouble(1,jobOrderID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                clientID = resultSet.getInt(1);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clientID;
    }
    //This method updates the payment date of a finished repair
    public void updatePayDate(int clerkID, int jobOrderID, LocalDate paymentDate, double totalCost){
        String queryString;
        String query = "SELECT joborder_id FROM receipt WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,jobOrderID);
            resultSet = preparedStatement.executeQuery();
            if(resultSet != null) {
                queryString = "UPDATE company.receipt SET pay_date = ? WHERE joborder_id = ?";
                preparedStatement = connection.prepareStatement(queryString);
                preparedStatement.setDate(1, java.sql.Date.valueOf(paymentDate));
                preparedStatement.setInt(2, jobOrderID);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } else {
                queryString = "INSERT INTO company.receipt(joborder_id,payment_processor,pay_date,fin_cost)" +
                        "VALUES(?,?,?,?)";
                preparedStatement = connection.prepareStatement(queryString);
                preparedStatement.setInt(1, jobOrderID);
                preparedStatement.setInt(2,clerkID);
                preparedStatement.setDate(3, java.sql.Date.valueOf(paymentDate));
                preparedStatement.setDouble(4, totalCost);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //This method returns a detail of a receipt for a particular job order
    public Receipt getReceiptDetails(int jobOrderID){
        Receipt details = null;
        String queryString = "SELECT * FROM company.receipt WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, jobOrderID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                details = new Receipt(resultSet.getInt(1), resultSet.getInt(2),
                        resultSet.getDate(3).toLocalDate(),resultSet.getDouble(4));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return details;
    }
    //This method returns a detail of a repair for a particular job order
    public Repair getRepairDetails(int jobOrderID){
        Repair details = null;
        String queryString = "SELECT * FROM company.repair WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, jobOrderID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                details =  new Repair(resultSet.getInt(1), resultSet.getInt(2),
                        String.valueOf(resultSet.getDate(3)), String.valueOf(resultSet.getDate(4)));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return details;
    }
    //This method returns na name of a staff
    public String getStaffName(int staffID){
        String queryString = "SELECT name FROM company.staff WHERE staff_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        String staffName = "";
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, staffID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                staffName = resultSet.getString(1);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staffName;
    }
    //This method returns the name of the technician who handled a particular job order
    public int getTechStaffID(int jobOrderID){
        String queryString = "SELECT technician FROM company.repair WHERE joborder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        int staffID = 0;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1, jobOrderID);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                staffID = resultSet.getInt(1);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staffID;
    }
    public void updateInventory(int partID, int quantity, int operation){
        String queryString;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            if(operation == 1){
                queryString = "UPDATE company.inventory SET stock = stock + ? WHERE part_id = ?";
                preparedStatement = connection.prepareStatement(queryString);
                preparedStatement.setInt(1, quantity);
                preparedStatement.setInt(2, partID);
                resultSet = preparedStatement.executeQuery();
                resultSet.close();
                preparedStatement.close();
            }
            if (operation == 2) {
                queryString = "UPDATE company.inventory SET stock = stock - ? WHERE part_id = ?";
                preparedStatement = connection.prepareStatement(queryString);
                preparedStatement.setInt(1, quantity);
                preparedStatement.setInt(2, partID);
                resultSet = preparedStatement.executeQuery();
                resultSet.close();
                preparedStatement.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void updateArriveDate(int partsID, LocalDate arriveDate){
        String queryString = "UPDATE company.partsorder SET arrive_Date = ? WHERE partsorder_id = ?";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try{
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setDate(1, java.sql.Date.valueOf(arriveDate));
            preparedStatement.setInt(2, partsID);
            resultSet = preparedStatement.executeQuery();
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
    public int getNewPartsOrderID() {
        String queryString = "SELECT MAX(partsorder_id) FROM partsorder";
        ResultSet resultSet;
        int newPartsOrderID = -1;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            while (resultSet.next()) {
                newPartsOrderID = resultSet.getInt(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newPartsOrderID + 1;
    }
    public void setNewPartsOrder(int jobOrderID, LocalDate orderDate){
        String queryString = "INSERT INTO company.partsOrder(partsorder_id, joborder_id, order_date, arrive_date)" +
                "VALUES(?,?,?,NULL)";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = connection.prepareStatement(queryString);
            preparedStatement.setInt(1,getNewPartsOrderID());
            preparedStatement.setInt(2,jobOrderID);
            preparedStatement.setDate(3,java.sql.Date.valueOf(orderDate));
            resultSet = preparedStatement.executeQuery();
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public List<PartsOrder> pendingPartsOrder(){
        List<PartsOrder> orderedParts = new ArrayList<PartsOrder>();

        String queryString = "SELECT * FROM company.partsorder WHERE arrive_date IS NULL";
        ResultSet resultSet;
        try {
            Statement query = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            resultSet = query.executeQuery(queryString);
            resultSet.beforeFirst();
            while (resultSet.next()) {

                PartsOrder partsOrder = new PartsOrder(resultSet.getInt(1), resultSet.getInt(2),
                        resultSet.getDate(3),
                        resultSet.getDate(4));
                orderedParts.add(partsOrder);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderedParts;
    }


}

